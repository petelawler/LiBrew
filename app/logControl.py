#! /usr/bin/env python3

"""Module docstring.

Log Control for librew
"""
from datetime import datetime

from logging import basicConfig, getLogger, ERROR
from globals import DEBUG_LEVEL, LogDir, LogDataFile, LogFileMode

basicConfig()
librew_log = getLogger('librew')
librew_log.setLevel(DEBUG_LEVEL)
librew_log.propagate = True
librew_log.info("logger constructed")
try:
    getLogger("werkzeug").setLevel(ERROR)
except Exception as e:
    print("Can't set Flask logging level: %s" % e)

def logdataHeader(sensorNum):
    ff = open(LogDir + LogDataFile + str(sensorNum) + ".csv",
              LogFileMode)
    ff.write("UTC,elapsed time,temperature,target,heat output\n")
    ff.close()

def logdata(brewtime, tank, temp, set_point, heat):
    f = open(LogDir + LogDataFile + str(tank) + ".csv", LogFileMode)
    f.write("%s,%s,%3.1f,%3.3f,%3.3f,%3.3f\n" % (datetime.utcnow(), brewtime,
                                              tank, temp, set_point, heat))
    f.close()


def logstatus(log_status_level, status_string):
    if log_status_level == "INFO":
        librew_log.info(status_string)
    elif log_status_level == "DEBUG":
        librew_log.debug(status_string)
    elif log_status_level == "ERROR":
        librew_log.error(status_string)
    elif log_status_level == "WARNING":
        librew_log.warning(status_string)
    elif log_status_level == "CRITICAL":
        librew_log.critical(status_string)


