#! /usr/bin/env python3

"""Module docstring.

Globals for librew
"""

from datetime import datetime as dt
from logging import basicConfig, getLogger, INFO, WARNING, DEBUG, ERROR
from os import chdir
from xml.etree import ElementTree as ET
from socket import gethostname

DB_DEBUG = False
APP_DEBUG = True
RUNTIME = dt.now()

# config loader
tree = ET.parse('config.xml')
xml_root = tree.getroot()

root_dir_elem = xml_root.find('RootDir')
if root_dir_elem is not None:
    chdir(root_dir_elem.text.strip())
else:
    librew_globals_log.debug("No RootDir tag found in config.xml, \
running from current directory")

DEBUG_LEVEL = xml_root.find('LogLevel').text.strip()
if DEBUG_LEVEL == "":
    DEBUG_LEVEL = WARNING
basicConfig()
librew_globals_log = getLogger('librew-globals')
librew_globals_log.setLevel(DEBUG_LEVEL)
librew_globals_log.propagate = True
librew_globals_log.debug("logger constructed")

template_name = xml_root.find('TemplateName').text.strip()
if template_name == "":
    template_name = "librew.html"

LogDir = xml_root.find('LogDir').text.strip()
if LogDir == "":
    LogDir = "/var/log/librew/"

LogDataFile = xml_root.find('LogDataFile').text.strip()
if LogDataFile == "":
    LogDataFile = "LiBrewData"

LogFileMode = xml_root.find('LogFileMode').text.strip()
if LogFileMode == "Overwrite":
    # See https://docs.python.org/2/library/functions.html#open
    LogFileMode = "w"
else:
    LogFileMode = "a"

    #    DBDir = xml_root.find('DBDir').text.strip()
    #    if DBDir == "":
    #        DBDir = "/var/lib/librew"
    #
    #    DBFile = xml_root.find('DBFile').text.strip()
    #    if DBFile == "":
    #        DBFile = "librew.db"
    #
    #    SQALCHEMY_FILE = DBDir + DBFile
    #    Engine = create_engine('sqlite:///' + SQALCHEMY_FILE, echo=DB_DEBUG)
    #    DBSession = sessionmaker(bind=Engine)
    #    session = DBSession()

gpioInverted = xml_root.find('GPIO_Inverted').text.strip()
if gpioInverted == "0":
    GPIO_ON = 1
    GPIO_OFF = 0
else:
    GPIO_ON = 0
    GPIO_OFF = 1

release_info = xml_root.find('Release_Info').text.strip()

librew_globals_log.debug("GPIO Inversion set: On = %s Off = %s" % (GPIO_ON, GPIO_OFF))

class param:
    """ Parameters that are used in the temperature control process. """
    status = {
        "numTempSensors": 0,
        "vesselList": "",
        "temp": "0",
        "tempUnits": "C",
        "elapsed": "0",
        "mode": "off",
        "cycle_time": 30.0,
        "duty_cycle": 0.0,
        "boil_duty_cycle": 60,
        "set_point": 0.0,
        "boil_manage_temp": 200,
        "num_pnts_smooth": 5,
        "k_param": 44,
        "i_param": 165,
        "d_param": 4
    }

hostname = gethostname()
