#!/usr/bin/python3 -I
#
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
# AGPLv3 ...
from librew import app

if __name__ == "__main__":
    app.run()
