#!/bin/bash

##########################################################################
# A script to install the LiBrew system
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##########################################################################

set -e -o pipefail

init () {
    DOWNLOAD_LOCATION=$HOME/Downloads
    INSTALL_LOCATION=/opt/LiBrew
    EXTRA_OVERLAYS_WGET_LOCATIONS=()
    BBDOTORG_OVERLAYS_GIT_LOCATION=https://github.com/BeagleBoard/bb.org-overlays.git
    PL_BB_OVERLAYS_URL=https://gitlab.com/petelawler/PL-BB-overlays/raw/master
#    PL_BB_OVERLAYS_ARRAY=(NEOSEC-TINYLCD35-00A0 PL-RELAY-ACTIVE-LOW-4PORT-00A0 PL-W1-P9.27-00A0 PL-UART4-00A0)
    PL_BB_OVERLAYS_ARRAY=(PL-W1-P9.27-00A0)
# MINIMUM_REQUIRED_KERNEL_VERSION="--lts-4_19"

    OS_ID="$(grep ID /etc/os-release |cut -f 2 -d =)"
    NOW="$(date +'%Y-%m-%d-%H-%M-%S')"
    USERID="$(id -u)"
    GROUPID="$(id -g)"

    RECHECK_OPERATIONS=""
    VERBOSE=""

    PYTHON_MINIMUM=3.6
    PYVERSION=$(python3 -c 'import sys; print(".".join(map(str, sys.version_info[:3])))'|cut -f 1,2 -d \.)
    if (( $(echo "$PYVERSION < $PYTHON_MINIMUM" | bc -l) )); then
        echo "Unsupported python version ($PYVERSION)"
    fi

    if [ "${UID}" != "0" ]; then
        /bin/echo "Not root user"
        if hash sudo 2>/dev/null; then
            HAVE_SUDO=$(command -v sudo 2>/dev/null)
            /bin/echo "sudo(1) found at $HAVE_SUDO"
        else
            /bin/echo "Need root/sudo access"
            $BEEP
            exit 1
        fi
    else
        /bin/echo "Running as root, sudo(1) not needed."
        HAVE_SUDO=""
    fi
    if [ -f "$(which timedatectl)" ]; then
        echo "Setting time via timedatectl"
        ${HAVE_SUDO} timedatectl set-ntp true
    else
        echo "Setting time via ntpdate"
        ${HAVE_SUDO} ntpdate pool.ntp.org
    fi
    echo "Testing for ${DOWNLOAD_LOCATION}"
    if [ ! -d ${DOWNLOAD_LOCATION} ]; then
        echo "Creating ${DOWNLOAD_LOCATION}"
        mkdir --parents ${VERBOSE} ${DOWNLOAD_LOCATION}
    fi
    ${HAVE_SUDO} apt update
}

services_stop () {
    echo "Testing for existing operations"
    check_operations=(apache2 bonescript-autorun bonescript.service bonescript.socket cloud9 cloud9.socket librew nginx node-red.service node-red.socket rc_battery_monitor robotcontrol dnsmasq.service node-red.socket wpa_supplicant.service)
    for service in "${check_operations[@]}"; do
        echo "Checking $service"
        if [ "$(systemctl is-active $service)" = "active" ]; then
            if [ ! -z $VERBOSE ]; then
                echo "Stopping $service operations"
            fi
            sudo systemctl stop $service;
        else
            if [ ! -z $VERBOSE ]; then
                echo "$service is already stopped"
            fi
        fi
        if [ "$(systemctl is-enabled $service)" = "enabled" ]; then
            if [ ! -z $VERBOSE ]; then
                echo "Disabling $service"
            fi
            sudo systemctl disable $service
        else
            if [ ! -z $VERBOSE ]; then
                echo "$service is already disabled"
            fi
        fi
    done
}

system_annoyances () {
     ${HAVE_SUDO} apt --assume-yes --purge remove bonescript c9-core-installer bb-node-red-installer  bb-beaglebone-io-installer bb-johnny-five-installer
     ${HAVE_SUDO} apt --assume-yes --purge autoremove
}

upgrade () {
    # TODO: check if upgrades are available
    while true; do
        read -p "Do you wish to perform system upgrades? ([y]es/[n]o) " yn
        case $yn in
            [Yy]* ) ${HAVE_SUDO} apt --assume-yes upgrade; \
                    RECHECK_OPERATIONS="Y"
                    break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

system_requirements () {
    # Install needed packages
    while true; do
    echo "Checking for support tools"
        read -p "Do you wish to check for tools? ([y]es/[n]o) " yn
            case $yn in
        [Yy]* )
            pkg=(python3-dev python3-pip python3-setuptools python3-virtualenv python3-venv)
            pkg=(${pkg[@]} build-essential bash-completion bison curl flex git git-core)
            pkg=(${pkg[@]} bb-cape-overlays i2c-tools watchdog)
            pkg=(${pkg[@]} libpcre3-dev libow-dev libsystemd-dev)
            pkg=(${pkg[@]} nginx-full)
            # shellshock disable=SC2086
            ${HAVE_SUDO} apt install ${pkg[@]}
            RECHECK_OPERATIONS="Y"
            break;;
        [Nn]* )
            break;;
        * )
            echo "Please answer yes or no.";;
        esac
    done
}

install_location_setup () {
    echo "Checking for old install"
    if [ -d "${INSTALL_LOCATION}" ]; then
        echo "Backing up old install"
        ${HAVE_SUDO} mv ${VERBOSE} "${INSTALL_LOCATION}" "${INSTALL_LOCATION}.${NOW}"
    fi

    echo "Creating install location"
    ${HAVE_SUDO} mkdir --parents ${VERBOSE} "${INSTALL_LOCATION}"
    if [ ! -d "${INSTALL_LOCATION}" ]; then
        echo "Creating ${INSTALL_LOCATION} failed"
        exit
    fi

    echo "Fixing permissions"
    ${HAVE_SUDO} chown ${VERBOSE} "${USERID}":"${GROUPID}" "${INSTALL_LOCATION}"
}

find_release_info () {
    GIT_BRANCH=$(git branch --no-color | cut -d \  -f 2 | tr -d '\n')
    GIT_COMMIT=$(git log -n1 --pretty='%h' | tr -d '\n')
    GIT_TAG=$(git describe --exact-match --tags ${GIT_COMMIT} 2>/dev/null | tr -d '\n')
    if [ -z $GIT_TAG ]; then
        GIT_TAG="tagless"
    fi
    echo "${GIT_BRANCH}-${GIT_COMMIT} ${GIT_TAG}"
    return
}

create_virtual_env () {
    echo "Checking for requirements.txt existence"
    if [ ! -r requirements.txt ] && [ ! -s requirements.txt ]; then
        echo "requirements.txt is broken"
        exit 1
    fi

    echo "Installing python $PYVERSION to ${INSTALL_LOCATION}"
    python3 -m venv --copies "${INSTALL_LOCATION}"

    # https://github.com/adafruit/adafruit-beaglebone-io-python/issues/308
    MYCFLAGS="CFLAGS=\"-Wno-cast-function-type -Wno-format-truncation -Wno-sizeof-pointer-memaccess -Wno-stringop-overflow\""

    echo "Establishing virtual environment in ${INSTALL_LOCATION}"
    bash -c "source ${INSTALL_LOCATION}/bin/activate && \
        pip3 install --upgrade pip wheel && \
        ${MYCFLAGS} pip3 install ${VERBOSE} Adafruit-BBIO &&
        pip3 install ${VERBOSE} -r requirements.txt"
}

overlays () {
    echo "Testing for ${DOWNLOAD_LOCATION}/bb.org-overlays/.git"
    if [ -d ${DOWNLOAD_LOCATION}/bb.org-overlays/.git ]; then
        echo "Updating bb.org-overlays if necessary"
        git -C ${DOWNLOAD_LOCATION}/bb.org-overlays reset --hard HEAD
        git -C ${DOWNLOAD_LOCATION}/bb.org-overlays pull
    else
        echo "Cloning bb.org-overlays"
        git -C ${DOWNLOAD_LOCATION} clone ${BBDOTORG_OVERLAYS_GIT_LOCATION}
    fi

# echo "Removing all device tree files"
# rm --force --recursive ${VERBOSE} "${DOWNLOAD_LOCATION}"/bb.org-overlays/src/arm/*

    echo "Fecthing custom overlays"
    overlay=""
    for overlay in "${PL_BB_OVERLAYS_ARRAY[@]}"; do
        echo "Overlay URL: $PL_BB_OVERLAYS_URL/$overlay"
        wget --continue --output-document ${DOWNLOAD_LOCATION}/bb.org-overlays/src/arm/$overlay.dts $PL_BB_OVERLAYS_URL/$overlay.dts
    done

    echo "Installing overlays"
    bash -c "cd ${DOWNLOAD_LOCATION}/bb.org-overlays && ./install.sh"
}

uenv () {
    sudo cp /boot/uEnv.txt /boot/uEnv.txt.$(date +'%Y-%m-%d-%H-%M-%S')

    echo "uname_r=$(uname -r)
enable_uboot_overlays=1
uboot_overlay_addr0=/lib/firmware/PL-W1-P9.27-00A0.dtbo
disable_uboot_overlay_emmc=1
disable_uboot_overlay_video=1
disable_uboot_overlay_audio=1
disable_uboot_overlay_wireless=1
disable_uboot_overlay_adc=1
cmdline=coherent_pool=1M
" | sudo tee /boot/uEnv.txt
}

startup () {
    echo "Installing systemd service"
    ${HAVE_SUDO} cp librew.service /etc/systemd/system/.
    ${HAVE_SUDO} chmod ${VERBOSE} 644 /etc/systemd/system/librew.service
    # use @ as a delimiter as INSTALL_LOCATION may contain the sed delimiter
    ${HAVE_SUDO} sed -i 's@INSTALL_LOCATION@'"$INSTALL_LOCATION"'@'g /etc/systemd/system/librew.service
    ${HAVE_SUDO} systemctl daemon-reload
    ${HAVE_SUDO} systemctl disable librew.service
}

logger () {
    echo "Checking for journald"
    if [ -f /etc/systemd/journald.conf ]; then
        if ! [ -d /etc/systemd/journald.conf.d ]; then
            ${HAVE_SUDO} mkdir ${VERBOSE} /etc/systemd/journald.conf.d
        fi
        ${HAVE_SUDO} bash -c "echo '[Journal]
Storage=persistent
SystemMaxUse=128M
' > /etc/systemd/journald.conf.d/00-librew.conf"
        ${HAVE_SUDO} systemctl restart systemd-journald
    fi
    echo "Checking for old logfiles"
    if [ -d /var/log/librew/ ]; then
        echo "Backing up old logfiles"
        ${HAVE_SUDO} mv ${VERBOSE} /var/log/librew/ /var/log/librew.${NOW}
    fi

    echo "Checking for missing logrotate configuration"
    if [ -f /etc/logrotate.d/librew ]; then
        echo "Installing logrotation"
${HAVE_SUDO} bash -c "echo '/var/log/librew/* {
rotate 5
weekly
notifempty
compress
}
' > /etc/logrotate.d/librew "
    fi
}

installer () {
    printf "Installing"
    ${HAVE_SUDO} cp --preserve=mode,ownership,timestamps --recursive ${VERBOSE} app/* config.xml ${INSTALL_LOCATION}
    printf "."
    RELEASE_INFO=$(find_release_info)
    ${HAVE_SUDO} bash -c "sed -i s/RELEASE_INFO/'${RELEASE_INFO}'/ ${INSTALL_LOCATION}/config.xml"
    if [ ! -d /var/log/librew/ ]; then
        printf "."
        ${HAVE_SUDO} mkdir --parents ${VERBOSE} /var/log/librew/
    fi
    printf "."
    printf ". done.\n"
}

# watchdog config
finish_up () {
    while true; do
        read -p "Do you wish to automatically boot LiBrew? " yn
        case $yn in
            [Yy]* ) ${HAVE_SUDO} systemctl enable librew.service;
            break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    while true; do
        read -p "Reboot to complete installation? " yn
        case $yn in
            [Yy]* ) ${HAVE_SUDO} systemctl reboot; break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

init
# Stop unwanted stuff now to free mem and ticks
services_stop
system_annoyances
upgrade
system_requirements
# Because sometimes updates reenable things
if [ "$RECHECK_OPERATIONS"="Y" ]; then
    services_stop
fi
install_location_setup
create_virtual_env
overlays
uenv
startup
logger
installer
finish_up
