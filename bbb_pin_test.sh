#!/bin/bash

# bbb_pintest.sh - a quick shell script to test BBB GPIO pins
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if ! hash config-pin 2>/dev/null; then
    echo "config-pin(1) not installed"
    exit 1
fi
OFF=hi
ON=lo
SLEEP_PERIOD=0.1
PIN_NUMBERS=(P8_07 P8_08 P8_09 P8_10)
for PIN_NUMBER in ${PIN_NUMBERS[*]}; do
    echo ${PIN_NUMBER};
    if [ -e /sys/devices/platform/ocp/ocp:${PIN_NUMBER}_pinmux ]; then
        ls /sys/devices/platform/ocp/ocp:${PIN_NUMBER}_pinmux
    fi
    echo "config-pin -q ${PIN_NUMBER}"
    config-pin -q ${PIN_NUMBER}
    sleep $SLEEP_PERIOD
    echo "config-pin -a ${PIN_NUMBER} OFF"
    config-pin -a ${PIN_NUMBER} ${OFF}
    sleep $SLEEP_PERIOD
    echo "config-pin -a ${PIN_NUMBER} ON"
    config-pin -a ${PIN_NUMBER} ${ON}
    sleep $SLEEP_PERIOD
    sleep $SLEEP_PERIOD
    echo "config-pin -a ${PIN_NUMBER} OFF"
    config-pin -a ${PIN_NUMBER} ${OFF}
done

